var booksCollModel = (function () {

    var _booksColl= [];



    function _readBooks() {
        var temp = window.localStorage["books"]

        if (!temp) _booksColl = [];
        else _booksColl = JSON.parse(temp);

        return _booksColl;
    }

    function _addbook(writerLastName, writerFirstName, writerBirt,bookName,pages,gener) {
        _booksColl.push({
            id: +_booksColl.length,
            writerLastName: writerLastName,
            writerFirstName: writerFirstName,
            writerBirt: writerBirt,
            bookName: bookName,
            pages: pages,
            gener: gener
        });
    }

    function _saveBook() {
        // второй параметр - функция, которая удаляет специальное свойство добавляемое angularJS для отслеживания дубликатов
        // http://mutablethought.com/2013/04/25/angular-js-ng-repeat-no-longer-allowing-duplicates/
        window.localStorage["books"] = JSON.stringify(_booksColl, function (key, val) {
            if (key == '$$hashKey') {
                return undefined;
            }
            return val
        });
    }



    function _removeBook(id) {
        _booksColl.forEach(function (e, index) {
            if (e.id == id) {
                _booksColl.splice(index, 1);
            }
        })
    }
//========================Geners===============================


var _geners = [];


    function _saveGeners() {
      // второй параметр - функция, которая удаляет специальное свойство добавляемое angularJS для отслеживания дубликатов
      // http://mutablethought.com/2013/04/25/angular-js-ng-repeat-no-longer-allowing-duplicates/
      window.localStorage["geners"] = JSON.stringify(_geners, function (key, val) {
          if (key == '$$hashKey') {
              return undefined;
          }
          return val
      });
    };


   function _defGener(){
     if(localStorage.getItem("geners") === null){
       var tempGener =[{"gener":"Бизнес"},
                     {"gener":"История"},
                     {"gener":"Для детей"},
                     {"gener":"Философия"}]
       // второй параметр - функция, которая удаляет специальное свойство добавляемое angularJS для отслеживания дубликатов
       // http://mutablethought.com/2013/04/25/angular-js-ng-repeat-no-longer-allowing-duplicates/
       window.localStorage["geners"] = JSON.stringify(tempGener, function (key, val) {
           if (key == '$$hashKey') {
               return undefined;
           }
           return val
       })}
   };

    function _addGener(gener) {
        _geners.push({
            id: +_geners.length,
            gener: gener
        });
    }




    function _readGeners() {
        var temp = window.localStorage["geners"]

        if (!temp) _geners = [];
        else _geners = JSON.parse(temp);

        return _geners;
    }

    // function update() {
    //
    // };


    return {
        removeBook:_removeBook,
        addGener: _addGener,
        defGener: _defGener,
        saveGeners:  _saveGeners,
        geners: _geners,
        readGeners: _readGeners,
        booksColl: _booksColl,
        readBooks: _readBooks,
        addbook: _addbook,
        saveBook: _saveBook
    };



})();
