
      angular.module("bookCollApp", [])
          .directive("limitTo", [function() {
                return {
                    restrict: "A",
                    link: function(scope, elem, attrs) {
                        var limit = parseInt(attrs.limitTo);
                        angular.element(elem).on("keypress", function(e) {
                            if (this.value.length == limit) e.preventDefault();
                        });
                    }
                }
          }])
          .controller("bookCollAppCtrl", function ($scope) {
            $scope.books = booksCollModel.readBooks();
            $scope.newBook = null;
            $scope.newGener = null;
            $scope.geners = booksCollModel.defGener();
            $scope.geners = booksCollModel.readGeners();
            $scope.pattern = /^[a-zа-яё]+$/i;
            $scope.patternGener = /^[а-я А-Я ё Ё a-z A-Z- ]+$/;
            $scope.patternDate= /^(0?[1-9]|[12][0-9]|3[01])[.](0?[1-9]|1[012])[.]\d{4}$/;
            $scope.patternBookName = /^[а-я А-Я ё Ё a-z A-Z 0-9]+$/;



            $scope.editBookClick = function () {
                booksCollModel.saveBook();
              };

            $scope.delBookClick = function ($index) {
                 $scope.books.splice($index, 1)
                booksCollModel.saveBook();
                };



            $scope.addBookClick = function () {
                booksCollModel.addbook(
                  $scope.newBook.writerLastName,
                  $scope.newBook.writerFirstName,
                  $scope.newBook.writerBirt.toLocaleDateString(),
                  $scope.newBook.bookName,
                  $scope.newBook.pages,
                  $scope.newGener.gener);
                booksCollModel.saveBook();

                $scope.resetForm();
            }

            $scope.resetForm = function() {
              $scope.newBook = null;

              $scope.bookForm.$setPristine();

            }
            $scope.resetGener = function() {
              $scope.newGener = null;
              $scope.formGener.$setPristine();

            }

            $scope.addgenersclick = function () {
                booksCollModel.addGener($scope.newGener.nGener);
                booksCollModel.saveGeners();
                $scope.resetGener();

            }



        });
